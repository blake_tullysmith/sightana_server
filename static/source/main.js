require.config({
    map: {
        '*': {
            'css': '../components/require-css/css'
        }
    },
    paths: {
        "system-css": "../styles/system",
        "system": "system",
        "bootstrap-css": "../components/bootstrap/dist/css/bootstrap.min",
        "bootstrap": "../components/bootstrap/dist/js/bootstrap.min",
        "jquery": "../components/jquery/dist/jquery.min"
    },

    shim : {
        "bootstrap" : { "deps" :['jquery'] }
    },
});

define(['system','bootstrap','css!bootstrap-css'], function (system) {

    system.initialize(document.querySelector("body"));

});