define(['css!system-css'], {

    addClass: function (element, className) {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    },

    initialize: function (element) {
        this.addClass(element, "container")
        console.log('test');
    }
});