package data.collectors;


import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

@Test()
public class CSVCollectorTest extends CSVCollector
{
    @Test
    public void testGetCSV() throws Exception
    {
        List<Map<String, Object>> content = this.getCSV("http://ichart.finance.yahoo.com/table.csv?s=AAPL&d=0&e=5&f=2010&g=d&a=0&b=4&c=2010");

        Assert.assertEquals(content.get(0).get("Date"), "2010-01-05");
    }
}
