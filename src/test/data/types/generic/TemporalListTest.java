package data.types.generic;

import data.types.patterns.accumulators.Accumulator;
import data.types.patterns.accumulators.CompleteAccumulator;
import data.types.patterns.wrappers.LabeledValue;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.time.Calendars;
import util.time.Milliseconds;

import java.util.Calendar;
import java.util.List;

import static util.Conversions.getCalendarDateOnly;

@Test()
public class TemporalListTest extends TemporalList<Integer>
{
    Accumulator<LabeledValue<Calendar, Integer>> accumulator = new CompleteAccumulator<>();

    @BeforeMethod
    public void setUp() throws Exception
    {
        clear();

        add(getCalendarDateOnly(Calendars.getNowDt()), 0);
        add(getCalendarDateOnly(Calendars.getOffsetAgoDt(Milliseconds.DAY)), 10);
        add(getCalendarDateOnly(Calendars.getOffsetAgoDt(Milliseconds.DAY*2)), 20);
        add(getCalendarDateOnly(Calendars.getOffsetAgoDt(Milliseconds.DAY*3)), 30);
        add(getCalendarDateOnly(Calendars.getOffsetAgoDt(Milliseconds.DAY*4)), 40);
    }

    @Test
    public void testSetFrustumNewestToOldest() throws Exception
    {
        long millis = System.currentTimeMillis();
        Calendar near = Calendar.getInstance();
        Calendar far = Calendar.getInstance();

        near.setTimeInMillis(millis - Milliseconds.DAY);
        far.setTimeInMillis(millis - (Milliseconds.DAY*3));

        setFrustum(getCalendarDateOnly(near), getCalendarDateOnly(far));

        accumulator.reset();

        entertain(accumulator);

        List<LabeledValue<Calendar, Integer>> entries = accumulator.getEntries();

        Assert.assertEquals((int)entries.get(0).getValue(),10);
        Assert.assertEquals((int)entries.get(1).getValue(),20);
        Assert.assertEquals((int)entries.get(2).getValue(),30);
    }

    @Test
    public void testSetFrustumOldesttoNewest() throws Exception
    {
        long millis = System.currentTimeMillis();
        Calendar near = Calendar.getInstance();
        Calendar far = Calendar.getInstance();

        near.setTimeInMillis(millis - (Milliseconds.DAY)*3);
        far.setTimeInMillis(millis - Milliseconds.DAY);

        setFrustum(getCalendarDateOnly(near), getCalendarDateOnly(far));

        accumulator.reset();

        entertain(accumulator);

        List<LabeledValue<Calendar, Integer>> entries = accumulator.getEntries();

        Assert.assertEquals((int)entries.get(0).getValue(),30);
        Assert.assertEquals((int)entries.get(1).getValue(),20);
        Assert.assertEquals((int)entries.get(2).getValue(),10);
    }

    @Test
    public void testEntertain() throws Exception
    {
        clearFrustum();

        accumulator.reset();

        entertain(accumulator);

        List<LabeledValue<Calendar, Integer>> entries = accumulator.getEntries();

        Assert.assertEquals((int)entries.get(0).getValue(),40);
        Assert.assertEquals((int)entries.get(1).getValue(),30);
        Assert.assertEquals((int)entries.get(2).getValue(),20);
        Assert.assertEquals((int)entries.get(3).getValue(),10);
        Assert.assertEquals((int)entries.get(4).getValue(),0);
    }

    @Test
    public void testGetSortedKeys() throws Exception
    {
        List<Calendar> keys = getSortedKeys();

        Assert.assertEquals(keys.get(4).getTimeInMillis(), Calendars.getNowDt().getTimeInMillis());
        Assert.assertEquals(keys.get(3).getTimeInMillis(), Calendars.getOffsetAgoDt(Milliseconds.DAY).getTimeInMillis());
        Assert.assertEquals(keys.get(2).getTimeInMillis(), Calendars.getOffsetAgoDt(Milliseconds.DAY*2).getTimeInMillis());
        Assert.assertEquals(keys.get(1).getTimeInMillis(), Calendars.getOffsetAgoDt(Milliseconds.DAY*3).getTimeInMillis());
        Assert.assertEquals(keys.get(0).getTimeInMillis(), Calendars.getOffsetAgoDt(Milliseconds.DAY*4).getTimeInMillis());
    }

    @Test
    public void testAdd() throws Exception
    {
        add(Calendars.getOffsetAgoDt(Milliseconds.DAY*200), 100);
        Assert.assertEquals((int) get(Calendars.getOffsetAgoDt(Milliseconds.DAY * 200)), 100);
    }

    @Test
    public void testRemove() throws Exception
    {
        clearFrustum();

        remove(getCalendarDateOnly(Calendars.getOffsetAgoDt(Milliseconds.DAY)));
        accumulator.reset();

        entertain(accumulator);

        List<LabeledValue<Calendar, Integer>> entries = accumulator.getEntries();

        Assert.assertEquals((int)entries.get(0).getValue(),40);
        Assert.assertEquals((int)entries.get(1).getValue(),30);
        Assert.assertEquals((int)entries.get(2).getValue(),20);
        Assert.assertEquals((int)entries.get(3).getValue(),0);
    }

    @Test
    public void testGet() throws Exception
    {
        Assert.assertEquals((int)get(Calendars.getNowDt()), 0);
    }

    @Test
    public void testClearFrustum() throws Exception
    {
        setFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), Calendars.getOffsetAgoDt(Milliseconds.DAY));
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 1);

        clearFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 5);
    }

    @Test
    public void testSize() throws Exception
    {
        Assert.assertTrue(size() == 5);
        clear();
        Assert.assertTrue(size() == 0);
    }

    @Test
    public void testGetBegining() throws Exception
    {
        Assert.assertEquals(getBegining().getTimeInMillis(), Calendars.getOffsetAgoDt(Milliseconds.DAY*4).getTimeInMillis());
    }

    @Test
    public void testGetEnding() throws Exception
    {
        Assert.assertEquals(getEnding().getTimeInMillis(), Calendars.getNowDt().getTimeInMillis());
    }

    @Test
    public void testSetBoundFrustum() throws Exception
    {
        clearFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 5);

        setBoundFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), FrustumBound.FRUSTUM_START);
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 4);

        setBoundFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), FrustumBound.FRUSTUM_END);
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 2);
    }

    @Test
    public void testSetPreviousFrustum() throws Exception
    {
        setFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), Calendars.getOffsetAgoDt(Milliseconds.DAY));
        clearFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 5);

        setPreviousFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 1);

        setFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), Calendars.getOffsetAgoDt(Milliseconds.DAY));
        clearFrustum();
        setFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), Calendars.getOffsetAgoDt(Milliseconds.DAY));

        setPreviousFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 5);

        setPreviousFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 1);

        setPreviousFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 1);

        setPreviousFrustum();
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 5);

    }

    @Test
    public void testSetFrustum() throws Exception
    {
        setFrustum(Calendars.getOffsetAgoDt(Milliseconds.DAY), Calendars.getOffsetAgoDt(Milliseconds.DAY));
        accumulator.reset();
        entertain(accumulator);
        Assert.assertTrue(accumulator.size() == 1);
    }

    @Test
    public void testClear() throws Exception
    {
        accumulator.reset();

        entertain(accumulator);

        Assert.assertTrue(accumulator.size() > 0);

        clear();
        accumulator.reset();

        entertain(accumulator);

        Assert.assertTrue(accumulator.size() == 0);
    }
}