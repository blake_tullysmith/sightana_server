package data.types.generic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SpanTest
{
    Span<Integer> instance;

    @Test
    public void testSpanConstructor() throws Exception
    {
        instance = new Span<>();

        Assert.assertNull(instance.getFrom());
        Assert.assertNull(instance.getTo());
    }

    @Test
    public void testSpanParameterConstructor() throws Exception
    {
        instance = new Span<>(10, 50);

        Assert.assertEquals((int) instance.getFrom(), 10);
        Assert.assertEquals((int) instance.getTo(), 50);
    }

    @Test
    public void testGetTo() throws Exception
    {
        instance = new Span<>(10, 50);

        Assert.assertEquals((int)instance.getTo(), 50);
    }

    @Test
    public void testSetTo() throws Exception
    {
        instance = new Span<>(10, 50);

        instance.setTo(15);

        Assert.assertEquals((int)instance.getTo(), 15);
    }

    @Test
    public void testGetFrom() throws Exception
    {
        instance = new Span<>(10, 50);

        Assert.assertEquals((int)instance.getFrom(), 10);
    }

    @Test
    public void testSetFrom() throws Exception
    {
        instance = new Span<>(10, 50);

        instance.setFrom(15);

        Assert.assertEquals((int)instance.getFrom(), 15);
    }
}