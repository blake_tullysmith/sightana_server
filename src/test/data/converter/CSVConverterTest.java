package data.converter;

import data.converters.CSVConverter;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Test()
public class CSVConverterTest extends CSVConverter
{
    @Test
    public void testGetNextField_onSingleFieldEncapsulatedField()
    {
        String field = "Some Field Value, including a comma, that we want to retrieve.";
        String input = String.format("\"%s\"", field);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);

        String resultStr = new String(result);

        Assert.assertEquals(resultStr, field);
    }

    @Test
    public void testGetNextField_onSingleFieldOpenField()
    {
        String field = "Some Field Value that we want to retrieve.";
        String input = String.format("%s", field);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);

        String resultStr = new String(result);

        Assert.assertEquals(resultStr, field);
    }

    @Test
    public void testGetNextField_onDualFieldOpenFields()
    {
        String fields[] = {
                "A",
                "B"
        };
        String input = String.format("%s,%s", fields[0],fields[1]);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);
        String resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[0]);

        result = this.getNextField(is);
        resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[1]);
    }

    @Test
    public void testGetNextField_onDualFieldEncapsulatedFields()
    {
        String fields[] = {
                "Some Field Value, including a comma, that we want to retrieve.",
                "Some Other Field, also with commas, and \"double-quotes\" for emphasis."
        };
        String input = String.format("\"%s\",\"%s\"", fields[0],fields[1].replace("\"", "\"\""));
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);
        String resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[0]);

        result = this.getNextField(is);
        resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[1]);
    }

    @Test
    public void testGetNextField_onDualFieldMixedFields()
    {
        String fields[] = {
                "Some Field Value, including a comma, that we want to retrieve.",
                "Some Other Field with \"double-quotes\" for emphasis."
        };
        String input = String.format("\"%s\",%s", fields[0],fields[1]);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);
        String resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[0]);

        result = this.getNextField(is);

        resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[1]);
    }

    @Test
    public void testGetNextField_onDualFieldMixedFieldsWithFollowingLine()
    {
        String fields[] = {
                "Some Field Value, including a comma, that we want to retrieve.",
                "Some Other Field with \"double-quotes\" for emphasis."
        };
        String input = String.format("\"%s\",%s\nField One,Field Two", fields[0],fields[1]);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        byte[] result = this.getNextField(is);
        String resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[0]);

        result = this.getNextField(is);

        resultStr = new String(result);

        Assert.assertEquals(resultStr, fields[1]);
    }

    @Test
    public void testGetRow_onDualFieldMixedFields()
    {
        String fields[] = {
                "Some Field Value, including a comma, that we want to retrieve.",
                "Some Other Field with \"double-quotes\" for emphasis."
        };
        String input = String.format("Field One,Field Two\n\"%s\",%s", fields[0],fields[1]);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        List<String> row = new ArrayList<>();

        this.getRow(is, row);

        Assert.assertEquals(row.size(), 2);

        row = new ArrayList<>();

        this.getRow(is, row);

        Assert.assertEquals(row.get(0), fields[0]);

        Assert.assertEquals(row.get(1), fields[1]);
    }

    @Test
    public void testConvert_onDualFieldMixedFields()
    {
        String fields[] = {
                "Some Field Value, including a comma, that we want to retrieve.",
                "Some Other Field with \"double-quotes\" for emphasis."
        };
        String input = String.format("Column One, Column Two\nField One,Field Two\n\"%s\",%s", fields[0],fields[1]);
        InputStream is = new ByteArrayInputStream(input.getBytes());
        List<Map<String, Object>> table;

        table = this.convert(is);

        Assert.assertEquals(table.size(), 2);

        Assert.assertEquals(table.get(0).get("Column One"), "Field One");
        Assert.assertEquals(table.get(0).get("Column Two"), "Field Two");
        Assert.assertEquals(table.get(1).get("Column One"), fields[0]);
        Assert.assertEquals(table.get(1).get("Column Two"), fields[1]);
    }
}
