package applications;

import applications.core.Application;
import org.apache.log4j.Logger;
import services.filesystem.watcher.FileSystemWatcherService;
import services.interfaces.api.APIPlaneService;
import services.interfaces.diagnostics.DiagnosticPlaneService;
import services.properties.Properties;
import services.properties.PropertyService;
import services.core.InvalidRootService;
import services.core.ServiceAlreadyRegistered;

public class SightanaServer extends Application
{
    private static Logger LOG = Logger.getLogger(applications.SightanaServer.class);

    private static final String NUMBER_OF_THREADS = "number-of-threads";
    private static final int DEFAULT_NUMBER_OF_THREADS = 5;

    private static final String NUMBER_OF_SCHEDULED_THREADS = "number-of-scheduled-threads";
    private static final int DEFAULT_NUMBER_OF_SCHEDULED_THREADS = 5;

    protected SightanaServer() throws ServiceAlreadyRegistered, InvalidRootService
    {
        super();

        register(null);

        new PropertyService("sightana-server");
        new FileSystemWatcherService("sightana-server");
        new APIPlaneService("sightana-server");
        new DiagnosticPlaneService("sightana-server");

        constructed();
    }

    @Override
    protected int numThreads()
    {
        return Properties.getIntProperty(NUMBER_OF_THREADS, DEFAULT_NUMBER_OF_THREADS);
    }

    @Override
    protected int numScheduledThreads()
    {
        return Properties.getIntProperty(NUMBER_OF_SCHEDULED_THREADS, DEFAULT_NUMBER_OF_SCHEDULED_THREADS);
    }

    @Override
    public String getName()
    {
        return "sightana-server";
    }

    public static void main(String[] args) throws ServiceAlreadyRegistered, InvalidRootService
    {
        LOG.info(String.format("STARTIING in %s", System.getProperty("user.dir")));
        LOG.info(String.format("APPLICATION In Thread %s INITIALIZING", Thread.currentThread().getName()));

        SightanaServerFactory.initialize();
        Application.getApplication().run(args);

        System.exit(0);
    }

}
