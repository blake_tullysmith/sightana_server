package applications.core;

import org.apache.log4j.Logger;
import services.core.ScheduledService;
import services.core.Service;
import services.core.Services;
import services.properties.PropertyService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

abstract public class Application extends Service
{
    private static Logger LOG = Logger.getLogger(Application.class);

    private ScheduledExecutorService scheduledExecutorService;
    private ExecutorService executor;

    public boolean isShutdown()
    {
        return shutdown;
    }

    private boolean shutdown = false;

    public String[] getParameters()
    {
        return parameters;
    }

    protected Application()
    {
        application = this;
        PropertyService.updateSystemProperties();
        addRootService(getName());
    }

    private String[] parameters;
    private List<String> validRootServices = new ArrayList<>();

    public void shutdown()
    {
        shutdown = true;
    }

    abstract protected int numThreads();

    abstract protected int numScheduledThreads();

    public final void scheduleService(ScheduledService service)
    {
        scheduledExecutorService.schedule(service, service.delay(), service.delayUnit());
    }

    public final void run(String[] args)
    {
        parameters = args;

        executor = Executors.newFixedThreadPool(numThreads());
        scheduledExecutorService = Executors.newScheduledThreadPool(numScheduledThreads());

        LOG.debug(String.format("APPLICATION In Thread %s STARTING SERVICES", Thread.currentThread().getName()));

        Services.startServices(executor);

        LOG.debug(String.format("APPLICATION In Thread %s STARTING EXECUTION", Thread.currentThread().getName()));

        while (!shutdown)
        {
            try
            {
                Thread.sleep(1);
            }
            catch (InterruptedException ignored)
            {
            }
        }

        synchronized(this)
        {
            LOG.debug(String.format("APPLICATION In Thread %s STOPPING EXECUTION", Thread.currentThread().getName()));
            Services.stopServices();
            LOG.debug(String.format("APPLICATION In Thread %s STOPPED SERVICE", Thread.currentThread().getName()));
            executor.shutdownNow();
            notify();
            scheduledExecutorService.shutdownNow();
            LOG.debug(String.format("APPLICATION In Thread %s SHUTTING DOWN THREAD POOL", Thread.currentThread().getName()));
            notify();
        }
    }

    protected final void addRootService(String name)
    {
        validRootServices.add(name);
    }

    public final boolean isValidRootService(String name)
    {
        return validRootServices.contains(name);
    }

    private static Application application = null;

    public static Application getApplication()
    {
        if (application == null)
        {
            ApplicationFactory.getFactory().getApplication();
        }

        return application;
    }

    public static File getWorkingDirectory()
    {
        return new File(System.getProperty("user.dir"));
    }


    @Override
    protected void notifyStarting() throws InterruptedException
    {
        started();
    }

    @Override
    protected void notifyStopping() throws InterruptedException
    {
        stopped();
    }

    @Override
    protected void notifyRunning() throws InterruptedException
    {
        while(getStatus() == Status.STARTED && !Application.getApplication().isShutdown())
        {
            LOG.debug(String.format("SERVICE %s In Thread %s RUNNING Application.notifyRunning", getName(), Thread.currentThread().getName()));
            yield();
        }
    }

}
