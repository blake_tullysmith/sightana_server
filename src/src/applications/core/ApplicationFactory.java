package applications.core;

import services.core.ServiceAlreadyRegistered;

public abstract class ApplicationFactory
{
    public static ApplicationFactory getFactory()
    {
        return factory;
    }

    protected void setFactory(ApplicationFactory factory)
    {
        if (ApplicationFactory.factory == null)
            ApplicationFactory.factory = factory;
    }

    protected static ApplicationFactory factory = null;

    public abstract Application getApplication() throws ServiceAlreadyRegistered;
}
