package applications;

import applications.core.Application;
import applications.core.ApplicationFactory;
import services.core.ServiceAlreadyRegistered;

public class SightanaServerFactory extends ApplicationFactory
{
    private SightanaServerFactory()
    {
        setFactory(this);
    }

    @Override
    public Application getApplication() throws ServiceAlreadyRegistered
    {
        return new SightanaServer();
    }

    public static void initialize()
    {
        new applications.SightanaServerFactory();
    }
}
