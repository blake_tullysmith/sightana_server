package services.core;

import applications.core.Application;
import data.types.patterns.visitor.Entertainer;
import data.types.patterns.visitor.Visitor;
import data.types.patterns.wrappers.LabeledValue;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class Services
{
    private static Logger LOG = Logger.getLogger(Services.class);

    private static Map<String, Service> services = new HashMap<>();

    public static Service getRunningService(Service caller, String name)
    {
        while (!services.containsKey(name))
        {
            try
            {
                caller.sleep(1);
            }
            catch (InterruptedException ignored)
            {
                Thread.currentThread().interrupt();
            }
        }

        return services.get(name);
    }

    public static void register(String name, String parent, Service instance) throws ServiceAlreadyRegistered
    {
        if (parent == null)
        {
            if (!Application.getApplication().isValidRootService(name))
                throw new InvalidRootService();
        }

        if (services.containsKey(name))
        {
            throw new ServiceAlreadyRegistered();
        }
        else
        {
            services.put(name, instance);
        }
    }

    public static void startServices(ExecutorService executor)
    {
        for(String service : services.keySet())
        {
            services.get(service).start(executor);
        }
    }

    public static void stopServices()
    {
        LOG.debug(String.format("SERVICES In Thread %s STOPPING SERVICES Services.stopServices", Thread.currentThread().getName()));

        for(String service : services.keySet())
        {
            LOG.debug(String.format("SERVICES In Thread %s STOPPING SERVICE %s Services.stopServices", Thread.currentThread().getName(), service));

            services.get(service).stop();
        }

        LOG.debug(String.format("SERVICES In Thread %s STOPPED SERVICES Services.stopServices", Thread.currentThread().getName()));

    }

    public static void entertain(Visitor<LabeledValue<String, Service>> guest)
    {
        for(String key : services.keySet())
        {
            guest.visit(new LabeledValue<>(key, services.get(key)));
        }
    }
}
