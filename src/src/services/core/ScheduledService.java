package services.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

abstract public class ScheduledService extends Service
{
    abstract public int delay();

    public TimeUnit delayUnit()
    {
        return TimeUnit.MILLISECONDS;
    }

    public void schedule(ExecutorService executor)
    {
        if (getStatus() == Status.CONSTRUCTED)
        {
            setStatus(Status.STARTING);
            executor.execute(this);
        }
    }
}
