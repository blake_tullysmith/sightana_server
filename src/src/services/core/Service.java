package services.core;

import applications.core.Application;

import data.types.patterns.wrappers.LabeledValue;
import org.apache.log4j.Logger;
import util.Instrumentation;
import util.Strings;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

abstract public class Service implements Runnable
{
    private static Logger LOG = Logger.getLogger(Service.class);

    public void start(ExecutorService executor)
    {
        if (status == Status.CONSTRUCTED)
        {
            status = Status.STARTING;
            executor.execute(this);
        }
    }

    public void stop()
    {
        status = Status.STOPPING;
    }

    Thread thread = null;

    @Override
    public void run()
    {
        LOG.info(String.format("SERVICE %s In Thread %s RUN", getName(), Thread.currentThread().getName()));

        thread = Thread.currentThread();

        try
        {
            while(status != Status.STOPPED)
            {
                switch (status)
                {
                    case STARTING:
                        LOG.debug(String.format("SERVICE %s In Thread %s STARTING", getName(), Thread.currentThread().getName()));
                        notifyStarting();
                        break;

                    case STARTED:
                        LOG.debug(String.format("SERVICE %s In Thread %s STARTED", getName(), Thread.currentThread().getName()));
                        notifyRunning();
                        status = Status.STOPPING;
                        break;

                    case STOPPING:
                        LOG.debug(String.format("SERVICE %s In Thread %s STOPPING", getName(), Thread.currentThread().getName()));
                        notifyStopping();
                        status = Status.STOPPED;
                        break;
                }
            }

        }
        catch (InterruptedException ignored)
        {
            Thread.currentThread().interrupt();
        }
    }

    public enum Status
    {
        CONSTRUCTING("CONSTRUCTING"),

        CONSTRUCTED("CONSTRUCTED"),

        STARTING("STARTING"),
        STARTED("STARTED"),
        STOPPING("STOPPING"),
        STOPPED("STOPPED"),

        SLEEPING("SLEEPING");


        private String label;


        Status(String label)
        {
            this.label = label;
        }

        @Override
        public String toString()
        {
            return label;
        }
    }

    protected void setStatus(Status status)
    {
        this.status = status;
    }

    private volatile Status status;

    public Status getStatus()
    {
        return status;
    }

    public String getThreadName()
    {
        if (thread != null)
        {
            return thread.getName();
        }
        else
        {
            return "UNKNOWN";
        }
    }

    public int getThreadPriority()
    {
        if (thread != null)
        {
            return thread.getPriority();
        }
        else
        {
            return -1;
        }
    }

    public String getThreadGroup()
    {
        if (thread != null)
        {
            return thread.getThreadGroup().getName();
        }
        else
        {
            return "UNKNOWN";
        }
    }

    public String[] getStackTrace()
    {
        if (thread != null)
        {
            List<String> accumulator = new ArrayList<>();
            StackTraceElement[] stackTraces = thread.getStackTrace();

            for(StackTraceElement stackTrace : stackTraces)
            {
                if (stackTrace.isNativeMethod())
                {
                    accumulator.add(String.format("(Native) %s ", stackTrace.toString()));
                }
                else
                {

                    accumulator.add(stackTrace.toString());
                }
            }

            String[] result = new String[accumulator.size()];

            accumulator.toArray(result);

            return result;
        }
        else
        {
            return new String[0];
        }
    }

    public Service()
    {
        this.status = Status.CONSTRUCTING;
    }

    public Service getRunningService(String name)
    {
        return Services.getRunningService(this, name);
    }

    public void constructed()
    {
        status = Status.CONSTRUCTED;

        LOG.debug(String.format("SERVICE %s In Thread %s CONSTRUCTED", getName(), Thread.currentThread().getName()));
    }

    public final void started()
    {
        status = Status.STARTED;
    }

    public final void stopped() throws InterruptedException
    {
        status = Status.STOPPED;

        LOG.debug(String.format("SERVICE %s In Thread %s STOPPED", getName(), Thread.currentThread().getName()));
        Thread.currentThread().join(1);
    }

    public String getPropertyName(String property)
    {
        return String.format("%s.%s",getName(), property);
    }

    public void yield() throws InterruptedException
    {
        LOG.debug(String.format("SERVICE %s In Thread %s YIELDING", getName(), Thread.currentThread().getName()));

        Thread.sleep(1);
        if (Application.getApplication().isShutdown())
        {
            Services.stopServices();
        }
    }

    public void sleep(long millis) throws InterruptedException
    {
        LOG.debug(String.format("SERVICE %s In Thread %s SLEEPING", getName(), Thread.currentThread().getName()));

        if (status == Status.STOPPING)
            return;

        Status previousStatus = status;
        status = Status.SLEEPING;
        Thread.sleep(millis);

        if (status == Status.SLEEPING)
            status = previousStatus;
    }

    public void register(String parent) throws ServiceAlreadyRegistered, InvalidRootService
    {
        Services.register(getName(), parent, this);
    }

    public List<LabeledValue<String, Object>> getInsturmentation()
    {
        List<LabeledValue<String, Object>> results =  new ArrayList<>();

        Field[] fields = this.getClass().getDeclaredFields();
        Method[] methods = this.getClass().getDeclaredMethods();

        for (Field field : fields)
        {
            if (field.isAnnotationPresent(Instrumentation.class))
            {
                try
                {
                    field.setAccessible(true);
                    results.add(new LabeledValue<>(Strings.pretty(field.getName()), field.get(this)));
                }
                catch (IllegalAccessException ignored)
                {
                }
            }
        }

        for (Method method : methods)
        {
            if (method.isAnnotationPresent(Instrumentation.class))
            {
                try
                {
                    method.setAccessible(true);
                    results.add(new LabeledValue<>(Strings.pretty(method.getName()), method.invoke(this)));
                }
                catch (InvocationTargetException | IllegalAccessException ignored)
                {
                }
            }
        }

        return results;
    }

    protected abstract String getName();
    protected abstract void notifyStarting() throws InterruptedException;
    protected abstract void notifyStopping() throws InterruptedException;
    protected abstract void notifyRunning() throws InterruptedException;
}
