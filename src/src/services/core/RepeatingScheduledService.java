package services.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

abstract public class RepeatingScheduledService extends ScheduledService
{
    private ScheduledExecutorService executorService = null;

    @Override
    public void schedule(ExecutorService executor)
    {
        super.schedule(executor);

        if (executor.getClass() == ScheduledExecutorService.class)
            executorService = (ScheduledExecutorService)executor;
    }

    @Override
    public void run()
    {
        super.run();

        if (executorService != null)
        {
            executorService.schedule(this, delay(), delayUnit());
        }
    }
}
