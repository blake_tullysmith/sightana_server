package services;

import applications.core.Application;

public class ShutdownService extends BaseScheduledService
{
    public static void shutdown()
    {
        Application.getApplication().scheduleService(new ShutdownService());
    }

    public ShutdownService()
    {
        super("shutdown", 100);

        constructed();
    }

    @Override
    protected void notifyRunning() throws InterruptedException
    {
        Application.getApplication().shutdown();
        stop();
    }
}
