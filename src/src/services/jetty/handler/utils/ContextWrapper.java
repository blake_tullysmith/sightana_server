package services.jetty.handler.utils;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ContextHandler;

public class ContextWrapper
{
    public static ContextHandler wrap(String context, Handler handler)
    {
        ContextHandler contextHandler = new ContextHandler(context);
        contextHandler.setHandler(handler);
        return contextHandler;
    }

    public static ContextHandler wrapAllowNullPathInfo(String context, Handler handler)
    {
        ContextHandler contextHandler = wrap(context, handler);
        contextHandler.setAllowNullPathInfo(true);
        return contextHandler;
    }
}
