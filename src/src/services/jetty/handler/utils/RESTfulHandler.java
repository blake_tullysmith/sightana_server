package services.jetty.handler.utils;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

abstract public class RESTfulHandler extends AbstractHandler
{
    public String getTarget()
    {
        return target;
    }

    private String target;

    public Request getBaseRequest()
    {
        return baseRequest;
    }

    private Request baseRequest;

    public HttpServletRequest getRequest()
    {
        return request;
    }

    private HttpServletRequest request;

    public HttpServletResponse getResponse()
    {
        return response;
    }

    private HttpServletResponse response;

    public boolean patch() throws IOException
    {
        return false;
    }

    public boolean put() throws IOException
    {
        return false;
    }

    public boolean post() throws IOException
    {
        return false;
    }

    public boolean get() throws IOException
    {
        return false;
    }

    public boolean delete() throws IOException
    {
        return false;
    }

    public boolean all() throws IOException
    {
        return false;
    }

    @Override
    public final void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        this.target = target;
        this.baseRequest = baseRequest;
        this.request = request;
        this.response = response;

        boolean handled = false;

        switch(request.getMethod())
        {
            case "PUT":
                handled = put();
                if(!handled)
                    handled = all();
                break;

            case "POST":
                handled = post();
                if(!handled)
                    handled = all();
                break;

            case "PATCH":
                handled = patch();
                if(!handled)
                    handled = all();
                break;

            case "DELETE":
                handled = delete();
                if(!handled)
                    handled = all();
                break;

            case "GET":
                handled = get();
                if(!handled)
                    handled = all();
                break;
        }

        getBaseRequest().setHandled(handled);
    }
}
