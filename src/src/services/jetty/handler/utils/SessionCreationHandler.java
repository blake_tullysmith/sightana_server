package services.jetty.handler.utils;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.session.JDBCSessionManager;
import util.time.Calendars;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

public class SessionCreationHandler extends AbstractHandler
{
    private static Logger LOG = Logger.getLogger(SessionCreationHandler.class);

    private final long sessionLife;

    public SessionCreationHandler(long sessionLife)
    {
        this.sessionLife = sessionLife;
    }

    private HttpSession getSession(HttpServletRequest request)
    {
        if (request.getSession(false) == null)
        {
            HttpSession session = request.getSession(true);
            if (session == null)
            {
                LOG.error("Unable to create session");
                return null;
            }

            session.setAttribute("created", new Date());
            session.setAttribute("expires", Calendars.getOffset(sessionLife).getTime());

            LOG.info(String.format("Created session %s", session.getId()));
        }

        return request.getSession();
    }

    private void clearExpiredSession(HttpSession session)
    {
        Date expires = (Date) session.getAttribute("expires");

        if (expires.getTime() < System.currentTimeMillis())
        {
            LOG.info(String.format("Clearing expired session %s", session.getId()));

            Enumeration<String> attributes = session.getAttributeNames();

            while (attributes.hasMoreElements())
            {
                session.removeAttribute(attributes.nextElement());
            }

            session.setAttribute("created", new Date());
            session.setAttribute("expires", Calendars.getOffset(sessionLife).getTime());
        }
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        HttpSession session = getSession(request);

        clearExpiredSession(session);
    }
}
