package services.jetty;

import edu.umd.cs.findbugs.annotations.Nullable;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.resource.ResourceCollection;
import services.BaseService;
import services.jetty.handler.utils.SessionCreationHandler;
import services.properties.PropertyService;

import javax.servlet.Servlet;
import java.util.HashMap;
import java.util.Map;

abstract public class JettyServerService extends BaseService
{
    private static Logger LOG = Logger.getLogger(JettyServerService.class);

    private final Server server;

    private PropertyService properties;

    private static String $PORT_NUMBER = "port-number";
    private static Map<String, Class<? extends Servlet>> servlets = new HashMap<>();

    private HandlerCollection postServeletHandlers;

    public JettyServerService(String name, String parent, @Nullable HandlerCollection postServeletHandlers)
    {
        super(name, parent);

        if (postServeletHandlers != null)
            this.postServeletHandlers = postServeletHandlers;
        else
            this.postServeletHandlers = new HandlerCollection();
        server = new Server();
    }

    protected void addHandler(Handler handler)
    {
        postServeletHandlers.addHandler(handler);
    }

    protected void addServlet(String uri, Class<? extends Servlet> c1ass)
    {
        servlets.put(uri, c1ass);
    }

    @Override
    protected void notifyStarting() throws InterruptedException
    {
        properties = (PropertyService) getRunningService("properties");

        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(properties.getIntegerProperty(getPropertyName($PORT_NUMBER), getDefaultPort()));
        server.setConnectors(new Connector[]{connector});

        HashSessionIdManager idmanager = new HashSessionIdManager();
        server.setSessionIdManager(idmanager);

        HandlerCollection handlers = new HandlerCollection();

        ContextHandler context = new ContextHandler("/");
        server.setHandler(context);

        HashSessionManager manager = new HashSessionManager();
        SessionHandler sessions = new SessionHandler(manager);
        context.setHandler(sessions);

        handlers.addHandler(context);
        handlers.addHandler(new SessionCreationHandler(getSessionExpiration()));

        for (Handler handler : postServeletHandlers.getHandlers())
        {
            handlers.addHandler(handler);
        }

        if (servlets.size() > 0)
        {
            ServletContextHandler svcContext = new ServletContextHandler();
            context.setContextPath("/");

            for (String uri : servlets.keySet())
            {
                svcContext.addServlet(servlets.get(uri), uri);
            }

            handlers.addHandler(context);
        }

        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        ResourceCollection resourceCollection = new ResourceCollection(getResourceDirs());
        resourceHandler.setBaseResource(resourceCollection);

        handlers.addHandler(resourceHandler);

        handlers.addHandler(new DefaultHandler());

        server.setHandler(handlers);
        try
        {
            server.setStopAtShutdown(true);
            server.setStopTimeout(0);
            server.start();
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Unable to start jetty", e);
        }
        super.notifyStarting();
    }

    protected abstract long getSessionExpiration();

    @Override
    protected void notifyStopping() throws InterruptedException
    {
        LOG.info("Stopping jetty");
        try
        {
            synchronized (server)
            {
                server.stop();
            }

            LOG.info("Stopped jetty");
        }
        catch (Exception e)
        {
            throw new RuntimeException("Unable to stop jetty", e);
        }
        stopped();
    }

    abstract public int getDefaultPort();

    public String[] getResourceDirs()
    {
        return new String[0];
    }
}
