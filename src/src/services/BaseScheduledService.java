package services;

import org.apache.log4j.Logger;
import services.core.ScheduledService;
import services.core.Service;

public class BaseScheduledService extends ScheduledService
{
    private static Logger LOG = Logger.getLogger(Service.class);

    private String name;
    private int millis;

    public BaseScheduledService(String name, int millis)
    {
        this.name = name;
        this.millis = millis;
    }

    @Override
    public void constructed()
    {
        super.constructed();
        super.setStatus(Status.STARTING);
    }

    @Override
    public int delay()
    {
        return millis;
    }

    @Override
    protected String getName()
    {
        return name;
    }

    @Override
    protected void notifyStarting() throws InterruptedException
    {
        started();
        LOG.info(String.format("SERVICE %s In Thread %s STARTED", getName(), Thread.currentThread().getName()));
    }

    @Override
    protected void notifyStopping() throws InterruptedException
    {
        stopped();
    }

    @Override
    protected void notifyRunning() throws InterruptedException
    {
        LOG.info(String.format("SERVICE %s In Thread %s RUNNING BaseScheduledService.notifyRunning", getName(), Thread.currentThread().getName()));
        yield();
    }
}
