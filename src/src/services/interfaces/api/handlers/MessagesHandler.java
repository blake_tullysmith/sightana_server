package services.interfaces.api.handlers;

import services.jetty.handler.utils.RESTfulHandler;

import java.io.IOException;

public class MessagesHandler extends RESTfulHandler
{
    @Override
    public boolean get() throws IOException
    {
        return super.get();
    }

    @Override
    public boolean put() throws IOException
    {
        return super.put();
    }

    @Override
    public boolean post() throws IOException
    {
        return super.post();
    }
}
