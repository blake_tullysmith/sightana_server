package services.interfaces.api;

import services.interfaces.api.handlers.MessagesHandler;
import services.jetty.JettyServerService;
import services.jetty.handler.utils.ContextWrapper;
import util.time.Milliseconds;

public class APIPlaneService extends JettyServerService
{
    public APIPlaneService(String parent)
    {
        super("api", parent, null);

        addHandler(ContextWrapper.wrap("/messages/", new MessagesHandler()));

        constructed();
    }

    @Override
    protected long getSessionExpiration()
    {
        return Milliseconds.HOUR*2;
    }

    @Override
    public int getDefaultPort()
    {
        return 8080;
    }
}
