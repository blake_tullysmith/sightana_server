package services.interfaces.diagnostics.handlers;

import services.ShutdownService;
import services.jetty.handler.utils.RESTfulHandler;
import util.markup.HTML;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ShutdownHandler extends RESTfulHandler
{
    @Override
    public boolean all() throws IOException
    {
        HttpServletResponse response = getResponse();

        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        PrintWriter out = response.getWriter();

        out.println(
                HTML.HTML(
                        HTML.BODY(
                                HTML.H1("Shutting down")
                        )
                )
        );

        ShutdownService.shutdown();

        return true;
    }
}
