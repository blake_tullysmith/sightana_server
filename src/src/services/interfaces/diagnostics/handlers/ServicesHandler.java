package services.interfaces.diagnostics.handlers;

import data.types.patterns.accumulators.CompleteAccumulator;
import data.types.patterns.visitor.Visitor;
import data.types.patterns.wrappers.LabeledValue;
import org.json.JSONWriter;
import services.core.Service;
import services.core.Services;
import services.jetty.handler.utils.RESTfulHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServicesHandler extends RESTfulHandler
{
    @Override
    public boolean get() throws IOException
    {
        HttpServletResponse response = getResponse();

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);

        JSONWriter writer = new JSONWriter(response.getWriter());

        CompleteAccumulator<LabeledValue<String, Service>> services =  new CompleteAccumulator<>();

        Services.entertain(services);

        writer = writer.array();

        for (LabeledValue<String, Service> service : services.getEntries())
        {
            Service svc = service.getValue();

            writer = writer.object()
                    .key("name").value(service.getLabel())
                    .key("status").value(svc.getStatus().toString())

                    .key("thread").value(svc.getThreadName())
                    .key("thread-group").value(svc.getThreadGroup())
                    .key("priority").value(svc.getThreadPriority());

            writer = writer.key("stack").array();

            for (String stackElement : svc.getStackTrace())
            {
                writer = writer.value(stackElement);
            }

            writer = writer.endArray();

            for (LabeledValue<String, Object> node : svc.getInsturmentation())
            {
                writer = writer.key(node.getLabel()).value(node.getValue().toString());
            }

            writer = writer.endObject();
        }

        writer.endArray();

        return true;
    }
}
