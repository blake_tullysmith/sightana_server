package services.interfaces.diagnostics;

import services.interfaces.diagnostics.handlers.ServicesHandler;
import services.interfaces.diagnostics.handlers.ShutdownHandler;
import services.jetty.JettyServerService;
import services.jetty.handler.utils.ContextWrapper;
import util.time.Milliseconds;

public class DiagnosticPlaneService extends JettyServerService
{
    public DiagnosticPlaneService(String parent)
    {
        super("diagnostics", parent, null);

        addHandler(ContextWrapper.wrap("/shutdown/", new ShutdownHandler()));
        addHandler(ContextWrapper.wrap("/services/", new ServicesHandler()));

        constructed();
    }

    @Override
    public String[] getResourceDirs()
    {
        return new String [] {
                String.format("%s/static", System.getProperty("user.dir"))
        };
    }

    @Override
    protected long getSessionExpiration()
    {
        return Milliseconds.HOUR*2;
    }

    @Override
    public int getDefaultPort()
    {
        return 9090;
    }
}
