package services.filesystem.watcher;

import java.io.File;

public interface FileSystemSubscriber
{
    void notifyFileSystemChange(File file, String change);
}
