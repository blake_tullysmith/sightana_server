package services.filesystem.watcher;

import org.apache.log4j.Logger;
import services.BaseService;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.nio.file.StandardWatchEventKinds;
import java.util.concurrent.TimeUnit;

public class FileSystemWatcherService extends BaseService
{
    private static Logger LOG = Logger.getLogger(FileSystemWatcherService.class);

    List<FileSystemSubscriber> subscribers = new ArrayList<>();

    /**
     * Creates a FileSystemWatcherService and registers the given directory
     */
    public FileSystemWatcherService(String parent)
    {
        super("filesystem-watcher", parent);

        try
        {
            this.watcher = FileSystems.getDefault().newWatchService();
        }
        catch (IOException e)
        {
            throw new RuntimeException(String.format("Unable to construct %s", getName()), e);
        }

        this.keys = new HashMap<WatchKey, Path>();

        constructed();
    }

    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event)
    {
        return (WatchEvent<T>) event;
    }

    public void addSubscriber(FileSystemSubscriber subscriber)
    {
        subscribers.add(subscriber);
    }

    public void removeSubscriber(FileSystemWatcherService subscriber)
    {
        subscribers.remove(subscribers.indexOf(subscriber));
    }

    public void addWatch(File file)
    {
        register(file.toPath());
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir)
    {
        WatchEvent.Kind<?>[] kinds = new WatchEvent.Kind[3];
        kinds[0] = StandardWatchEventKinds.ENTRY_CREATE;
        kinds[1] = StandardWatchEventKinds.ENTRY_DELETE;
        kinds[2] = StandardWatchEventKinds.ENTRY_MODIFY;

        File file = dir.toAbsolutePath().toFile();

        if (file.isFile())
            dir = new File(file.getParent()).toPath();

        WatchKey key = null;
        try
        {
            key = dir.toAbsolutePath().register(watcher, kinds);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException
    {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Override
    protected void notifyStopping() throws InterruptedException
    {
        try
        {
            watcher.close();
        }
        catch (IOException ignored)
        {
        }

        stopped();
    }

    /**
     * Process all events for keys queued to the watcher
     */
    @Override
    protected void notifyRunning() throws InterruptedException
    {
        while(getStatus() == Status.STARTED)
        {
            LOG.debug(String.format("SERVICE %s In Thread %s RUNNING notifyRunning", getName(), Thread.currentThread().getName()));

            // wait for key to be signalled
            WatchKey key = watcher.poll(1000, TimeUnit.MILLISECONDS);

            if (key != null)
            {
                Path dir = keys.get(key);
                if (dir != null)
                {

                    for (WatchEvent<?> event : key.pollEvents())
                    {
                        WatchEvent.Kind kind = event.kind();

                        // TBD - provide example of how OVERFLOW event is handled
                        if (kind != StandardWatchEventKinds.OVERFLOW)
                        {

                            // Context for directory entry event is the file name of entry
                            WatchEvent<Path> ev = cast(event);
                            Path name = ev.context();
                            Path child = dir.resolve(name);

                            for (FileSystemSubscriber subscriber : subscribers)
                            {
                                subscriber.notifyFileSystemChange(child.toFile(), event.kind().name());
                            }

                            // if directory is created, and watching recursively, then
                            // register it and its sub-directories
                            if (kind == StandardWatchEventKinds.ENTRY_CREATE)
                            {
                                try
                                {
                                    if (Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS))
                                    {
                                        registerAll(child);
                                    }
                                }
                                catch (IOException x)
                                {
                                    // ignore to keep sample readbale
                                }
                            }

                        }
                    }
                }

                // reset key and remove from set if directory no longer accessible
                boolean valid = key.reset();
                if (!valid)
                {
                    keys.remove(key);

                    // all directories are inaccessible
                    if (keys.isEmpty())
                    {
                        break;
                    }
                }
            }

            yield();
        }
    }
}
