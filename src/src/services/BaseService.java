package services;

import applications.core.Application;
import org.apache.log4j.Logger;
import services.core.Service;

public class BaseService extends Service
{
    private static Logger LOG = Logger.getLogger(Service.class);

    private String name;

    public BaseService(String name, String parent)
    {
        super();
        this.name = name;
        register(parent);
    }

    @Override
    protected String getName()
    {
        return name;
    }

    @Override
    protected void notifyStarting() throws InterruptedException
    {
        started();
    }

    @Override
    protected void notifyStopping() throws InterruptedException
    {
        stopped();
    }

    @Override
    protected void notifyRunning() throws InterruptedException
    {
        while(getStatus() == Status.STARTED && !Application.getApplication().isShutdown())
        {
            LOG.debug(String.format("SERVICE %s In Thread %s RUNNING BaseService.notifyRunning", getName(), Thread.currentThread().getName()));
            yield();
        }
    }
}
