package services.properties;

public class Properties
{
    public static String getStringProperty(String name, String def)
    {
        return System.getProperty(name, def);
    }

    public static int getIntProperty(String name, int def)
    {
        return Integer.parseInt(getStringProperty(name, Integer.toString(def)));
    }
}
