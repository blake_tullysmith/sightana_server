package services.properties;

import org.apache.log4j.Logger;
import services.BaseService;
import services.filesystem.watcher.FileSystemSubscriber;
import services.filesystem.watcher.FileSystemWatcherService;
import util.Instrumentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService extends BaseService implements FileSystemSubscriber
{
    private static Logger LOG = Logger.getLogger(PropertyService.class);

    public static String SETTINGS_CONFIG_FILENAME = "settings.cfg";
    Properties properties = new Properties();

    FileSystemWatcherService fileSystemWatcherService = null;

    public PropertyService(String parent)
    {
        super("properties", parent);
        constructed();
    }

    public static void updateSystemProperties()
    {
        java.util.Properties props = PropertyService.getProperties();

        for (String prop : props.stringPropertyNames())
        {
            System.setProperty(prop, props.getProperty(prop));
        }
    }

    public String getStringProperty(String name, String def)
    {
        return properties.getProperty(name, def);
    }

    public int getIntegerProperty(String name, int def)
    {
        return Integer.parseInt(getStringProperty(name, Integer.toString(def)));
    }

    public static Properties getProperties()
    {
        Properties properties = new Properties();
        InputStream input = null;

        try
        {
            File settingsFile = new File(SETTINGS_CONFIG_FILENAME);
            input = new FileInputStream(settingsFile);
            properties.load(input);

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if (input != null)
            {
                try
                {
                    input.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return properties;
    }

    private void loadProperties()
    {
        properties = PropertyService.getProperties();
    }

    @Override
    protected void notifyStarting() throws InterruptedException
    {
        LOG.debug(String.format("SERVICE %s In Thread %s RUNNING notifyStarting", getName(), Thread.currentThread().getName()));

        fileSystemWatcherService = (FileSystemWatcherService) getRunningService("filesystem-watcher");

        fileSystemWatcherService.addSubscriber(this);
        fileSystemWatcherService.addWatch(new File(SETTINGS_CONFIG_FILENAME));

        super.notifyStarting();
    }

    @Override
    public void notifyFileSystemChange(File file, String change)
    {
        LOG.debug(String.format("SERVICE %s In Thread %s RUNNING notifyFileSystemChange", getName(), Thread.currentThread().getName()));

        if (file.getName().endsWith(SETTINGS_CONFIG_FILENAME))
        {
            loadProperties();
            PropertyService.updateSystemProperties();
        }
    }

    @Instrumentation
    public String _properties()
    {
        String accum = "Properties\n\n";

        for(String property : properties.stringPropertyNames())
        {
            accum += String.format("%s: %s\n", property, properties.getProperty(property));
        }

        accum += "System Properties\n\n";

        for(String property : System.getProperties().stringPropertyNames())
        {
            accum += String.format("%s: %s\n", property, System.getProperty(property));
        }

        return accum;
    }
}
