package data.collectors;

import java.net.*;
import java.io.*;

/*
General URL harvester used by CSVCollector which is used by SymbolCollector to get data from http://ichart.finance.yahoo.com/table.csv?s=^DJI&d=0&e=28&f=2010&g=d&a=3&b=12&c=1996&ignore=.csv
 */

public class URLCollector
{
    public String getText(String url) throws Exception
    {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine).append("\n");

        in.close();

        return response.toString();
    }
}
