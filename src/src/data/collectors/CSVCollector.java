package data.collectors;

import data.converters.CSVConverter;

import java.util.List;
import java.util.Map;

public class CSVCollector extends URLCollector
{
    private CSVConverter convertor = new CSVConverter();

    public List<Map<String, Object>> getCSV(String url) throws Exception
    {
        return this.getCSV(url, true, true);
    }

    public List<Map<String, Object>> getCSV(String url, boolean firstLineIsHeader, boolean includeHeaderlessFields) throws Exception
    {
        String raw = this.getText(url);

        return convertor.convert(raw, firstLineIsHeader, includeHeaderlessFields);
    }
}
