package data.converters;

import java.io.*;
import java.util.*;

public class CSVConverter
{
    private enum CSVReadState
    {
        NEW_FIELD,
        OPEN_FIELD,
        ENCAPSULATED_FIELD,
        END_OF_ROW,
        END_OF_TABLE
    }

    public static final char DOUBLE_QUOTE_CHARACTER = '"';
    public static final char COMMA_CHARACTER = ',';

    private char quoteChar;
    private char commaChar;
    private char qouteEscapeChar;

    public CSVConverter()
    {
        this(DOUBLE_QUOTE_CHARACTER, COMMA_CHARACTER, DOUBLE_QUOTE_CHARACTER);
    }

    public CSVConverter(char quoteChar, char commaChar, char quoteEscapeCharacter)
    {
        this.quoteChar = quoteChar;
        this.commaChar = commaChar;
        this.qouteEscapeChar = quoteEscapeCharacter;
    }

    private byte[] getBytesFromByteList(List<Byte> list)
    {
        byte[] result = new byte[list.size()];

        for (int index = 0; index < list.size(); index++)
        {
            result[index] = list.get(index);
        }

        return result;
    }

    private CSVReadState readState = CSVReadState.NEW_FIELD;

    protected byte[] getNextField(InputStream input)
    {
        List<Byte> result = new ArrayList<>();

        try
        {
            readState = CSVReadState.NEW_FIELD;
            int nextByte = input.read();

            if (nextByte == 0x0D || nextByte == 0x0A)
            {
                nextByte = input.read();
            }

            if(nextByte == -1)
            {
                readState = CSVReadState.END_OF_TABLE;
                return new byte[0];
            }

            while (nextByte != -1)
            {
                if (nextByte == this.qouteEscapeChar && readState == CSVReadState.ENCAPSULATED_FIELD)
                {
                    nextByte = input.read();

                    // Handle escapements
                    if (nextByte == this.quoteChar)
                    {
                        result.add((byte) nextByte);
                        nextByte = input.read();
                    }
                    else if(this.qouteEscapeChar == this.quoteChar)
                    {
                        // That's the end of the field
                        return getBytesFromByteList(result);
                    }
                    else
                    {
                        // Escape character used directly
                        result.add((byte) nextByte);

                        // No advancement as that occurred with the forward look.
                    }
                }
                else
                {
                    if (nextByte == this.quoteChar)
                    {
                        switch (readState)
                        {
                            case NEW_FIELD:
                                readState = CSVReadState.ENCAPSULATED_FIELD;
                                nextByte = input.read();
                                break;

                            case ENCAPSULATED_FIELD:
                                // That's the end of the field
                                return getBytesFromByteList(result);

                            case OPEN_FIELD:
                                result.add((byte) nextByte);
                                nextByte = input.read();
                                break;
                        }
                    }
                    else if (nextByte == this.commaChar)
                    {
                        switch (readState)
                        {
                            case OPEN_FIELD:
                                // That's the end of the field
                                return getBytesFromByteList(result);
                            case ENCAPSULATED_FIELD:
                                result.add((byte) nextByte);
                                nextByte = input.read();
                                break;

                        }
                    }
                    else
                    {
                        if (readState == CSVReadState.NEW_FIELD)
                        {
                            readState = CSVReadState.OPEN_FIELD;
                        }

                        result.add((byte) nextByte);
                        nextByte = input.read();
                    }
                }

                if (nextByte == 0x0D || nextByte == 0x0A)
                {
                    readState = CSVReadState.END_OF_ROW;
                    return getBytesFromByteList(result);
                }
            }
        }
        catch (IOException ignored)
        {
            readState = CSVReadState.END_OF_TABLE;
        }

        return getBytesFromByteList(result);
    }

    public List<Map<String, Object>> convert(String input)
    {
        return convert(input, true, true);
    }

    public List<Map<String, Object>> convert(String input, boolean firstLineIsHeader, boolean includeHeaderlessFields)
    {
        InputStream is = new ByteArrayInputStream(input.getBytes());

        return convert(is, firstLineIsHeader, includeHeaderlessFields);
    }

    public List<Map<String, Object>> convert(InputStream is)
    {
        return convert(is, true, true);
    }

    public List<Map<String, Object>> convert(InputStream is, boolean firstLineIsHeader, boolean includeHeaderlessFields)
    {
        List<Map<String, Object>> result = new ArrayList<>();

        List<String> headers = new ArrayList<>();

        readState = CSVReadState.NEW_FIELD;

        if (firstLineIsHeader)
        {
            getRow(is, headers);
        }

        while (readState != CSVReadState.END_OF_TABLE)
        {
            List<String> row = new ArrayList<>();

            getRow(is, row);

            Map<String, Object> resolvedRow = new HashMap<>();

            for (int column = 0; column < row.size(); column++)
            {
                if (column < headers.size())
                {
                    resolvedRow.put(headers.get(column), row.get(column));
                }
                else
                {
                    if (includeHeaderlessFields)
                    {
                        resolvedRow.put(Integer.toString(column), row.get(column));
                    }
                }
            }

            result.add(resolvedRow);
        }

        return result;
    }

    protected void getRow(InputStream is, Collection<String> row)
    {
        while (readState != CSVReadState.END_OF_ROW && readState != CSVReadState.END_OF_TABLE)
        {
            byte[] fieldBytes = getNextField(is);
            String field = new String(fieldBytes);

            field = field.trim();

            row.add(field);
        }

        if (readState == CSVReadState.END_OF_ROW)
            readState = CSVReadState.NEW_FIELD;
    }

}
