package data.types.patterns.visitor;

public interface Visitor<T>
{
    public void visit(T host);
}
