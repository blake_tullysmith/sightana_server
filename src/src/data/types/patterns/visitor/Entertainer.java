package data.types.patterns.visitor;

public interface Entertainer<T>
{
    public void entertain(Visitor<T> guest);
}
