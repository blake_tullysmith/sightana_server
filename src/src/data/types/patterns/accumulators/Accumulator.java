package data.types.patterns.accumulators;

import data.types.patterns.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

public abstract class Accumulator<T> implements Visitor<T>
{
    protected List<T> entries = new ArrayList<>();

    public int size()
    {
        return entries.size();
    }

    public List<T> getEntries()
    {
        return entries;
    }

    @Override
    public void visit(T host)
    {
        if (collect(host))
            entries.add(host);
    }

    public abstract boolean collect(T host);

    public void reset()
    {
        entries.clear();
    }
}
