package data.types.patterns.accumulators;

public class CompleteAccumulator<T> extends Accumulator<T>
{
    @Override
    public boolean collect(T host)
    {
        return true;
    }
}
