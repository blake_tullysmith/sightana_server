package data.types.patterns.accumulators;

import data.types.patterns.wrappers.LabeledValue;

abstract public class CompleteListing<L, V> extends ListingAccumulator<L, V>
{
    @Override
    public boolean collect(LabeledValue<L, V> host)
    {
        return true;
    }
}
