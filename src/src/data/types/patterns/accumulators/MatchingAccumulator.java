package data.types.patterns.accumulators;

import data.types.patterns.Matcher;

public abstract class MatchingAccumulator<L,R> extends Accumulator<R> implements Matcher<L, R>
{
    private final L target;

    public MatchingAccumulator(L target)
    {
        this.target = target;
    }

    public L getTarget()
    {
        return target;
    }

    @Override
    public boolean collect(R host)
    {
        return false;
    }
}
