package data.types.patterns.accumulators;

import data.types.patterns.wrappers.LabeledValue;

abstract public class ListingAccumulator<L, V> extends Accumulator<LabeledValue<L, V>>
{
    @Override
    public String toString()
    {
        String result = "";

        for (LabeledValue<L, V> entry : getEntries())
        {
            if (result != "")
                result += "\n";

            result += format(entry.getLabel(), entry.getValue());
        }

        return result;
    }

    abstract public String format(L label, V value);
}
