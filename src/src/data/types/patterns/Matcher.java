package data.types.patterns;

public interface Matcher<L, R>
{
    public boolean matches(L left, R right);
}
