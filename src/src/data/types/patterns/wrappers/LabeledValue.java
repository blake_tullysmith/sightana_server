package data.types.patterns.wrappers;

public class LabeledValue<L, V>
{
    protected L label;
    protected V value;

    public LabeledValue(L label, V value)
    {
        this.label = label;
        this.value = value;
    }

    public V getValue()
    {
        return value;
    }

    public L getLabel()
    {
        return label;
    }
}
