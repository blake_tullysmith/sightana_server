package data.types.generic;

public class Span<T>
{
    protected T from;
    protected T to;

    public Span()
    {
        from = null;
        to = null;
    }

    public Span(T from, T to)
    {
        this.from = from;
        this.to = to;
    }

    public T getTo()
    {
        return to;
    }

    public void setTo(T to)
    {
        this.to = to;
    }

    public T getFrom()
    {

        return from;
    }

    public void setFrom(T from)
    {
        this.from = from;
    }
}
