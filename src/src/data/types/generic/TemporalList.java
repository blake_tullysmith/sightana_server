package data.types.generic;

import data.types.patterns.visitor.Entertainer;
import data.types.patterns.visitor.Visitor;
import data.types.patterns.wrappers.LabeledValue;
import data.types.time.CalendarSpan;

import java.util.*;

public class TemporalList<T> implements Entertainer<LabeledValue<Calendar, T>>
{
    public enum FrustumBound
    {
        FRUSTUM_START,
        FRUSTUM_END
    }

    public TemporalList()
    {
        frustumHistory.push(null);
    }

    public void clear()
    {
        frustumHistory = new Stack<>();
        entries = new HashMap<>();
        frustum = null;
    }

    protected Map<Calendar, T> entries = new HashMap<>();
    protected List<Calendar> frustum = null;
    protected Stack<CalendarSpan> frustumHistory = new Stack<>();

    public List<Calendar> getSortedKeys()
    {
        if (sortedKeys == null)
        {
            sortedKeys = new ArrayList<>(entries.keySet());
            Collections.sort(sortedKeys);
        }

        return sortedKeys;
    }

    protected List<Calendar> sortedKeys = null;

    public void add(Calendar key, T value)
    {
        sortedKeys = null;
        entries.put(key, value);
    }

    public void remove(Calendar key)
    {
        sortedKeys = null;
        entries.remove(key);
    }

    public T get(Calendar at)
    {
        return entries.get(at);
    }

    public void clearFrustum()
    {
        frustum = null;
        frustumHistory.push(null);
    }

    public int size()
    {
        return entries.size();
    }

    public Calendar getBegining()
    {
        return Collections.min(getSortedKeys());
    }

    public Calendar getEnding()
    {
        return Collections.max(getSortedKeys());
    }

    public void setBoundFrustum(Calendar cutoff, FrustumBound bound)
    {
        Calendar near, far;

        if (bound == FrustumBound.FRUSTUM_END)
        {
            near = cutoff;
            far = getEnding();
        }
        else
        {
            near = getBegining();
            far = cutoff;
        }

        setFrustum(near, far);
    }

    public void setPreviousFrustum()
    {
        if (frustumHistory.size() >= 2)
        {
            frustumHistory.pop(); // Current
            CalendarSpan frustum = frustumHistory.pop();

            if (frustum != null)
                setFrustum(frustum.getFrom(), frustum.getTo());
            else
                clearFrustum();
        }
        else
        {
            // previous frustum was null;
            if (frustumHistory.size() > 0)
            {
                frustumHistory.pop();
            }

            clearFrustum();
        }
    }

    public void setFrustum(Calendar near, Calendar far)
    {
        List<Calendar> keys = getSortedKeys();

        int nearIndex = Collections.binarySearch(keys, near);
        int farIndex = Collections.binarySearch(keys, far);

        frustum = keys.subList(Math.min(nearIndex, farIndex), Math.max(nearIndex, farIndex)+1);

        if(near.compareTo(far) == 1)
        {
            Collections.reverse(frustum);
        }

        frustumHistory.push(new CalendarSpan(near, far));
    }

    @Override
    public void entertain(Visitor<LabeledValue<Calendar, T>> guest)
    {
        if (frustum == null)
        {
            frustum = new ArrayList<>(entries.keySet());
            Collections.sort(frustum);
        }

        for(Calendar key : frustum)
        {
            guest.visit(new LabeledValue<>(key, entries.get(key)));
        }
    }
}
