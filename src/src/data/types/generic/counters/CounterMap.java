package data.types.generic.counters;

import data.types.patterns.wrappers.LabeledValue;
import data.types.patterns.visitor.Entertainer;
import data.types.patterns.visitor.Visitor;

import java.util.HashMap;
import java.util.Map;

public class CounterMap<T> implements Entertainer<LabeledValue<T, Double>>
{
    private Map<T, Long> counts = new HashMap<>();
    private long total = 0;
    private T mostProbable = null;

    public T getMostProbable()
    {
        if (mostProbable == null)
        {
            double highestProbable = 0;

            for(T instance : counts.keySet())
            {
                double currentProbable = probabilityOf(instance);

                if (currentProbable > highestProbable)
                {
                    highestProbable = currentProbable;
                    mostProbable = instance;
                }
            }
        }

        return mostProbable;
    }

    public void add (T instance)
    {
        long currentCount = 0;

        if (counts.containsKey(instance))
            currentCount = counts.get(instance);

        currentCount++;

        counts.put(instance, currentCount);
        total++;

        mostProbable = null;
    }

    public long countOf(T instance)
    {
        if (counts.containsKey(instance))
            return counts.get(instance);
        else
            return 0;
    }

    public double probabilityOf(T instance)
    {
        if (total > 0)
            return ((double)countOf(instance))/((double)total);
        else
            return 0.0;
    }

    @Override
    public void entertain(Visitor<LabeledValue<T, Double>> guest)
    {
        for(T key : counts.keySet())
        {
            LabeledValue<T, Double> labeledValue = new LabeledValue<>(key, probabilityOf(key));
            guest.visit(labeledValue);
        }
    }
}
