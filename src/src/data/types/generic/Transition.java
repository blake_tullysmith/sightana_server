package data.types.generic;

public class Transition<T>
{
    private final T from;
    private final T to;

    public Transition(T from, T to)
    {
        this.from = from;
        this.to = to;
    }

    public T getFrom()
    {
        return from;
    }

    public T getTo()
    {
        return to;
    }
}
