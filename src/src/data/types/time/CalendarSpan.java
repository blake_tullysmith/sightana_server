package data.types.time;

import data.types.generic.Span;

import java.util.Calendar;

public class CalendarSpan extends Span<Calendar>
{
    public CalendarSpan()
    {
        super();
    }

    public CalendarSpan(Calendar from, Calendar to)
    {
        super(from, to);
    }
}
