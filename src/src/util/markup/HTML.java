package util.markup;

import java.util.HashMap;
import java.util.Map;

public class HTML
{
    public static String tag(String tag, String value, String ... args)
    {
        Map<String, String> attributes = new HashMap<>();
        String attribs = "";

        String key = null;

        for (String arg : args)
        {
            if (key == null)
            {
                key = arg;
            }
            else
            {
                attributes.put(key, arg);
                key = null;
            }
        }

        for (String attribute : attributes.keySet())
        {
            attribs += String.format(" %s=\"%s\" ", attribute, attributes.get(attribute).replace("\"", "\\\""));
        }

        return String.format("<%s %s>%s</%s>", tag, attribs, value, tag);
    }

    public static String H1(String value, String ... args)
    {
        return tag("H1", value, args);
    }

    public static String HTML(String value, String ... args)
    {
        return tag("HTML", value, args);
    }

    public static String BODY(String value, String ... args)
    {
        return tag("BODY", value, args);
    }
}
