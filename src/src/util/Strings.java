package util;

public class Strings
{
    public static String[] none()
    {
        return new String[0];
    }

    public static String[] wrap(String str)
    {
        String[] result = new String[1];
        result[0] = str;
        return result;
    }

    public static String pretty(String str)
    {
        String result = str;

        while (result.startsWith("_"))
        {
            result = result.substring(1);
        }

        result = result.substring(0, 1).toUpperCase() +  result.substring(1);

        return result;
    }
}
