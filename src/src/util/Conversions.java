package util;

import java.util.Calendar;

public class Conversions
{
    public static Calendar getCalendarDateOnly(Calendar cal)
    {
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
        return cal;
    }

    public static double getDouble(Object obj) throws Exception
    {
        if (obj == null)
            return 0;
        else if (obj.getClass() == String.class)
            return Double.parseDouble((String)obj);
        else if (obj.getClass() == Double.class)
            return (Double)obj;
        else if (obj.getClass() == Long.class)
            return (Long)obj;
        else if (obj.getClass() == Integer.class)
            return (Integer)obj;
        else
            throw new Exception(String.format("Unable to convert %s with value of %s to double",
                    obj.getClass().toString(), obj.toString()));
    }

    public static long getLong(Object obj) throws Exception
    {
        if (obj == null)
            return 0;
        else if (obj.getClass() == String.class)
            return Long.parseLong((String)obj);
        else if (obj.getClass() == Double.class)
            return ((Double)obj).longValue();
        else if (obj.getClass() == Long.class)
            return (Long)obj;
        else if (obj.getClass() == Integer.class)
            return (Integer)obj;
        else
            throw new Exception(String.format("Unable to convert %s with value of %s to long",
                    obj.getClass().toString(), obj.toString()));
    }
}
