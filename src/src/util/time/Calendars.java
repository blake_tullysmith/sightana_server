package util.time;

import util.Conversions;

import java.util.Calendar;

public class Calendars
{
    public static Calendar getMillis(long millis)
    {
        Calendar cal;

        cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);

        return cal;
    }

    public static Calendar getNow()
    {
        return getMillis(System.currentTimeMillis());
    }

    public static Calendar getOffset(long millis)
    {
        return getMillis(System.currentTimeMillis()+millis);
    }

    public static Calendar getOffsetAgo(long millis)
    {
        return getOffset(-millis);
    }

    public static Calendar getNowDt()
    {
        return Conversions.getCalendarDateOnly(getNow());
    }

    public static Calendar getOffsetDt(long millis)
    {
        return Conversions.getCalendarDateOnly(getOffset(millis));
    }

    public static Calendar getOffsetAgoDt(long millis)
    {
        return Conversions.getCalendarDateOnly(getOffsetAgo(millis));
    }

    public static Calendar getMillisDt(long millis)
    {
        return Conversions.getCalendarDateOnly(getMillis(millis));
    }

}
