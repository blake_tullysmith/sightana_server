package util.time;

public class Milliseconds
{
    public static final long SECOND = 1000;
    public static final long MINUTE = 60*SECOND;
    public static final long HOUR = 60*MINUTE;
    public static final long DAY = 24*HOUR;
    public static final long WEEK = DAY*7;
    public static final long YEAR = (long)(365.25*(double)DAY);
    public static final long MONTH = (long)(((double)YEAR)/12.0);
}
